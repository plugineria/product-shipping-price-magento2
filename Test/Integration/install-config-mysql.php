<?php

declare(strict_types=1);

use Magento\Setup\Model\SearchConfigOptionsList;
use Magento\TestFramework\Bootstrap;

$config = [
    'db-host' => $_ENV['MYSQL_HOST'],
    'db-user' => $_ENV['MYSQL_USER'],
    'db-password' => $_ENV['MYSQL_PASSWORD'],
    'db-name' => 'magento_test_integration',
    'db-prefix' => '',
    'backend-frontname' => 'backend',
    'admin-user' => Bootstrap::ADMIN_NAME,
    'admin-password' => Bootstrap::ADMIN_PASSWORD,
    'admin-email' => Bootstrap::ADMIN_EMAIL,
    'admin-firstname' => Bootstrap::ADMIN_FIRSTNAME,
    'admin-lastname' => Bootstrap::ADMIN_LASTNAME,
    'amqp-host' => $_ENV['RABBITMQ_HOST'],
    'amqp-port' => $_ENV['RABBITMQ_PORT'],
    'amqp-user' => $_ENV['RABBITMQ_USER'],
    'amqp-password' => $_ENV['RABBITMQ_PASSWORD'],
];

if (class_exists(SearchConfigOptionsList::class)) {
    $config = array_merge(
        $config,
        [
            'elasticsearch-host' => $_ENV['ELASTICSEARCH_HOST'],
            'elasticsearch-port' => $_ENV['ELASTICSEARCH_PORT'],
        ]
    );
}

return $config;
