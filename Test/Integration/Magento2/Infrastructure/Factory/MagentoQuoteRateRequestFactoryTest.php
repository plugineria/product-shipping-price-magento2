<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Factory;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Factory\MagentoQuoteRateRequestFactory;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote\AddressFactory;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote\Item;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote\QuoteFactory;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use TddWizard\Fixtures\Catalog\ProductBuilder;
use TddWizard\Fixtures\Catalog\ProductFixture;

class MagentoQuoteRateRequestFactoryTest extends TestCase
{
    private const DEST_COUNTRY = 'PL';
    private const DEST_REGION_CODE = 'PL-06';
    private const DEST_REGION_NAME = 'lubelskie';
    private const DEST_CITY = 'Lublin';
    private const DEST_POSTCODE = '22-555';
    private const DEST_STREET = '3 Maja 15';

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store shipping/origin/country_id PL
     * @magentoConfigFixture current_store shipping/origin/region_id 692
     * @magentoConfigFixture current_store shipping/origin/postcode 00-001
     * @magentoConfigFixture current_store shipping/origin/city Warszawa
     * @magentoConfigFixture current_store currency/options/default EUR
     */
    public function testSimpleProductWithoutDimensionsReturnsRateRequestWithoutDimensions(): void
    {
        // Arrange
        /** @var MagentoQuoteRateRequestFactory $factory */
        $factory = Bootstrap::getObjectManager()->get(MagentoQuoteRateRequestFactory::class);
        $productPrice = 19.99;
        $productFixture = new ProductFixture(
            ProductBuilder::aSimpleProduct()
                ->withPrice($productPrice)
                ->build()
        );
        $productId = new MagentoProductId($productFixture->getId());
        $shippingAddress = self::provideShippingAddress();
        $expectedItem = self::provideItem($productFixture->getId(), $shippingAddress);

        // Act
        $rateRequest = $factory->create($productId, $shippingAddress);

        // Assert
        self::assertEquals(
            new RateRequest([
                'all_items' => [$expectedItem],
                'orig_country_id' => 'PL',
                'orig_region_id' => 692,
                'orig_postcode' => '00-001',
                'orig_city' => 'Warszawa',

                'dest_country_id' => self::DEST_COUNTRY,
                'dest_region_id' => $rateRequest->getDestRegionId(),
                'dest_region_code' => self::DEST_REGION_CODE,
                'dest_postcode' => self::DEST_POSTCODE,
                'dest_city' => self::DEST_CITY,
                'dest_street' => self::DEST_STREET,

                'free_method_weight' => 0,
                'store_id' => '1',
                'website_id' => '1',
                'base_currency' => 'USD',

                'package_value' => number_format($productPrice, 6),
                'package_value_with_discount' => number_format($productPrice, 6),
                'package_qty' => 1,
                'package_physical_value' => 1,
                'package_currency' => 'EUR',
                'package_weight' => null,
            ]),
            $rateRequest
        );

        // Tear Down
        $productFixture->rollback();
    }

    private static function provideItem(int $productId, Address $shippingAddress): Item
    {
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = Bootstrap::getObjectManager()->get(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->getById($productId);

        /** @var QuoteFactory $quoteFactory */
        $quoteFactory = Bootstrap::getObjectManager()->get(QuoteFactory::class);
        $quote = $quoteFactory->create();

        /** @var AddressFactory $addressFactory */
        $addressFactory = Bootstrap::getObjectManager()->get(AddressFactory::class);
        $quoteAddress = $addressFactory->create($shippingAddress, $quote);

        return new Item($product, $quoteAddress, $quote);
    }

    private static function provideShippingAddress(): Address
    {
        return AddressTestBuilder::create()
            ->setCountry(self::DEST_COUNTRY)
            ->setRegion(self::DEST_REGION_NAME)
            ->setCity(self::DEST_CITY)
            ->setPostalCode(self::DEST_POSTCODE)
            ->setStreetAddress(self::DEST_STREET)
            ->build();
    }
}
