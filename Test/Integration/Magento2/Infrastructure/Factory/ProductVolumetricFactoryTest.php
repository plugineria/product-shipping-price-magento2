<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Factory;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Factory\ProductVolumetricFactory;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Volumetric;
use Plugineria\ProductShippingPrice\Test\Integration\Fixture\ConfigurableProductFixture;
use TddWizard\Fixtures\Catalog\ProductBuilder;
use TddWizard\Fixtures\Catalog\ProductFixture;

class ProductVolumetricFactoryTest extends TestCase
{
    public function testCreateForSimpleProductWithWeightReturnsVolumetricWithWeight(): void
    {
        // Arrange
        /** @var ProductVolumetricFactory $factory */
        $factory = Bootstrap::getObjectManager()->get(ProductVolumetricFactory::class);
        $weight = 2;
        $productFixture = new ProductFixture(
            ProductBuilder::aSimpleProduct()
                ->withWeight($weight)
                ->build()
        );
        $productId = new MagentoProductId($productFixture->getId());

        // Act
        $volumetric = $factory->create($productId);

        // Assert
        self::assertEquals(
            new Volumetric($weight),
            $volumetric
        );

        // Tear down
        $productFixture->rollback();
    }

    public function testCreateForSimpleProductWithoutWeightReturnsEmptyVolumetric(): void
    {
        // Arrange
        /** @var ProductVolumetricFactory $factory */
        $factory = Bootstrap::getObjectManager()->get(ProductVolumetricFactory::class);
        $productFixture = new ProductFixture(
            ProductBuilder::aSimpleProduct()
                ->build()
        );
        $productId = new MagentoProductId($productFixture->getId());

        // Act
        $volumetric = $factory->create($productId);

        // Assert
        self::assertEquals(
            new Volumetric(),
            $volumetric
        );

        // Tear down
        $productFixture->rollback();
    }

    /**
     * @magentoDataFixture loadConfigurableProduct
     */
    public function testCreateForConfigurableProductWithChildrenWeightReturnsVolumetricWithFirstChildWeight(): void
    {
        // Arrange
        /** @var ProductVolumetricFactory $factory */
        $factory = Bootstrap::getObjectManager()->get(ProductVolumetricFactory::class);

        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = Bootstrap::getObjectManager()->get(ProductRepositoryInterface::class);
        $configurableProduct = $productRepository->get('configurable');
        $productId = new MagentoProductId((int)$configurableProduct->getId());

        // Act
        $volumetric = $factory->create($productId);

        // Assert
        self::assertEquals(
            new Volumetric(ConfigurableProductFixture::WEIGHT_BY_SKU['simple_10']),
            $volumetric
        );
    }

    public static function loadConfigurableProduct(): void
    {
        ConfigurableProductFixture::load();
    }

    public static function loadConfigurableProductRollback(): void
    {
        ConfigurableProductFixture::rollback();
    }
}
