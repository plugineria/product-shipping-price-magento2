<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Service\ShippingAddress;

use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
// phpcs:ignore Generic.Files.LineLength
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingAddress\MagentoExampleShippingAddressResolver;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;

class MagentoExampleShippingAddressResolverTest extends TestCase
{
    private const REGION = 'California';

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store shipping/origin/country_id PL
     * @magentoConfigFixture current_store shipping/origin/city Warszawa
     * @magentoConfigFixture current_store shipping/origin/postcode 00-001
     */
    public function testWhenExampleShippingAddressIsNotConfiguredThenValuesTakenFromOriginShippingAddress(): void
    {
        // Arrange
        /** @var MagentoExampleShippingAddressResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoExampleShippingAddressResolver::class);

        // Act + assert
        self::assertEquals(
            AddressTestBuilder::create()
                ->setCountry('PL')
                ->setPostalCode('00-001')
                ->setCity('Warszawa')
                ->setRegion(self::REGION)
                ->setStreetAddress(null)
                ->build(),
            $resolver->getExampleShippingAddress()
        );
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store plugineria_product_shipping_price/example_shipping_address/country PL
     * @magentoConfigFixture current_store plugineria_product_shipping_price/example_shipping_address/postcode 00-001
     * @magentoConfigFixture current_store plugineria_product_shipping_price/example_shipping_address/city Warszawa
     * @magentoConfigFixture current_store plugineria_product_shipping_price/example_shipping_address/street Koszykowa 1
     */
    public function testWhenExampleShippingAddressIsConfiguredThenValuesReturned(): void
    {
        // Arrange
        /** @var MagentoExampleShippingAddressResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoExampleShippingAddressResolver::class);

        // Act + assert
        self::assertEquals(
            AddressTestBuilder::create()
                ->setCountry('PL')
                ->setPostalCode('00-001')
                ->setCity('Warszawa')
                ->setRegion(self::REGION)
                ->setStreetAddress('Koszykowa 1')
                ->build(),
            $resolver->getExampleShippingAddress()
        );
    }
}
