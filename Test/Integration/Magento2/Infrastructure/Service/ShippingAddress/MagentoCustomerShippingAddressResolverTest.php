<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Service\ShippingAddress;

use Magento\Directory\Model\Region;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\DefaultCustomerId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoCustomerId;
// phpcs:ignore Generic.Files.LineLength
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingAddress\MagentoCustomerShippingAddressResolver;
use TddWizard\Fixtures\Customer\AddressBuilder;
use TddWizard\Fixtures\Customer\CustomerBuilder;
use TddWizard\Fixtures\Customer\CustomerFixture;

class MagentoCustomerShippingAddressResolverTest extends TestCase
{
    private const REGION1 = 'mazowieckie';
    private const REGION2 = 'lubelskie';

    public function testWhenCustomerHas2AddressesReturnMarkedAsDefaultShipping(): void
    {
        // Arrange
        $region1 = self::provideRegionIdByName(self::REGION1);
        $region2 = self::provideRegionIdByName(self::REGION2);

        /** @var MagentoCustomerShippingAddressResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoCustomerShippingAddressResolver::class);
        $customerFixture = new CustomerFixture(
            CustomerBuilder::aCustomer()
                ->withAddresses(
                    AddressBuilder::anAddress()
                        ->withCountryId('PL')
                        ->withRegionId($region1)
                        ->withCity('Warszawa')
                        ->withPostcode('00-001')
                        ->withStreet('Koszykowa 1'),
                    AddressBuilder::anAddress()
                        ->asDefaultShipping()
                        ->withCountryId('PL')
                        ->withRegionId($region2)
                        ->withCity('Lublin')
                        ->withPostcode('22-555')
                        ->withStreet('3 Maja 15'),
                )
                ->build()
        );
        $customerId = new DefaultCustomerId((string)$customerFixture->getId());

        // Act
        $primaryCustomerAddress = $resolver->getPrimaryCustomerShippingAddress($customerId);

        // Assert
        self::assertEquals(
            new DefaultAddress(
                'PL',
                'Lublin',
                '22-555',
                '3 Maja 15',
                self::REGION2,
            ),
            $primaryCustomerAddress->getAddress()
        );
        self::assertEquals(
            new MagentoCustomerId((int)$customerFixture->getId()),
            $primaryCustomerAddress->getCustomerId()
        );

        // Tear down
        $customerFixture->rollback();
    }

    public function testWhenCustomerHas2AddressesAndNoneMarkedForShippingReturnFirstFromList(): void
    {
        // Arrange
        $region1 = self::provideRegionIdByName(self::REGION1);
        $region2 = self::provideRegionIdByName(self::REGION2);

        /** @var MagentoCustomerShippingAddressResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoCustomerShippingAddressResolver::class);
        $customerFixture = new CustomerFixture(
            CustomerBuilder::aCustomer()
                ->withAddresses(
                    AddressBuilder::anAddress()
                        ->asDefaultShipping()
                        ->withCountryId('PL')
                        ->withRegionId($region2)
                        ->withCity('Lublin')
                        ->withPostcode('22-555')
                        ->withStreet('3 Maja 15'),
                    AddressBuilder::anAddress()
                        ->asDefaultBilling()
                        ->withCountryId('PL')
                        ->withRegionId($region1)
                        ->withCity('Warszawa')
                        ->withPostcode('00-001')
                        ->withStreet('Koszykowa 1'),
                )
                ->build()
        );
        $customerId = new DefaultCustomerId((string)$customerFixture->getId());

        // Act
        $primaryCustomerAddress = $resolver->getPrimaryCustomerShippingAddress($customerId);

        // Assert
        self::assertEquals(
            new DefaultAddress(
                'PL',
                'Lublin',
                '22-555',
                '3 Maja 15',
                self::REGION2,
            ),
            $primaryCustomerAddress->getAddress()
        );
        self::assertEquals(
            new MagentoCustomerId((int)$customerFixture->getId()),
            $primaryCustomerAddress->getCustomerId()
        );

        // Tear down
        $customerFixture->rollback();
    }

    public function testWhenCustomerDoesNotHaveAddressesReturnNone(): void
    {
        // Arrange
        /** @var MagentoCustomerShippingAddressResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoCustomerShippingAddressResolver::class);
        $customerFixture = new CustomerFixture(
            CustomerBuilder::aCustomer()
                ->build()
        );
        $customerId = new DefaultCustomerId((string)$customerFixture->getId());

        // Act + assert
        self::assertNull($resolver->getPrimaryCustomerShippingAddress($customerId));

        // Tear down
        $customerFixture->rollback();
    }

    private static function provideRegionIdByName(string $name): int
    {
        /** @var Region $region1 */
        $region1 = Bootstrap::getObjectManager()->create(Region::class);

        return (int)$region1->loadByName($name, 'PL')->getId();
    }
}
