<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Service\ShippingRate;

use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\DefaultProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\DefaultShippingRate;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingRate\MagentoPriceCalculator;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use TddWizard\Fixtures\Catalog\ProductBuilder;
use TddWizard\Fixtures\Catalog\ProductFixture;

class MagentoPriceCalculatorTest extends TestCase
{
    /** @var MagentoPriceCalculator */
    private $priceCalculator;

    protected function setUp(): void
    {
        $this->priceCalculator = Bootstrap::getObjectManager()->create(MagentoPriceCalculator::class);
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/flatrate/price 14.99
     */
    public function testGetRatesForActiveFlatRateCarrierAndSimpleAvailableProductReturnsFixedRate(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('flatrate');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals(
            [
                new DefaultShippingRate(
                    $shippingMethodId,
                    'flatrate',
                    14.99,
                    'Fixed'
                ),
            ],
            $rates,
        );

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     */
    public function testGetRatesForActiveFlatRateCarrierAndVirtualProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('flatrate');
        $virtualProduct = new ProductFixture(
            ProductBuilder::aVirtualProduct()->build()
        );
        $productId = new DefaultProductId((string)$virtualProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $virtualProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/flatrate/active 0
     */
    public function testGetRatesForInactiveFlatRateCarrierAndSimpleProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('flatrate');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture default_store carriers/dhl/active 1
     * @magentoConfigFixture default_store carriers/dhl/id some ID
     * @magentoConfigFixture default_store carriers/dhl/shipment_days Mon,Tue,Wed,Thu,Fri,Sat
     * @magentoConfigFixture default_store carriers/dhl/intl_shipment_days Mon,Tue,Wed,Thu,Fri,Sat
     * @magentoConfigFixture default_store carriers/dhl/allowed_methods IE
     * @magentoConfigFixture default_store carriers/dhl/international_service IE
     * @magentoConfigFixture default_store carriers/dhl/gateway_url https://xmlpi-ea.dhl.com/XMLShippingServlet
     * @magentoConfigFixture default_store carriers/dhl/id some ID
     * @magentoConfigFixture default_store carriers/dhl/password some password
     * @magentoConfigFixture default_store carriers/dhl/content_type N
     * @magentoConfigFixture default_store carriers/dhl/nondoc_methods 1,3,4,8,P,Q,E,F,H,J,M,V,Y
     * @magentoConfigFixture default_store carriers/dhl/showmethod 1
     * @magentoConfigFixture default_store carriers/dhl/title DHL Title
     * @magentoConfigFixture default_store carriers/dhl/specificerrmsg dhl error message
     * @magentoConfigFixture default_store carriers/dhl/unit_of_measure K
     * @magentoConfigFixture default_store carriers/dhl/size 1
     * @magentoConfigFixture default_store carriers/dhl/height 1.6
     * @magentoConfigFixture default_store carriers/dhl/width 1.6
     * @magentoConfigFixture default_store carriers/dhl/depth 1.6
     * @magentoConfigFixture default_store carriers/dhl/debug 1
     * @magentoConfigFixture default_store shipping/origin/country_id GB
     */
    public function testGetRatesForActiveDhlCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('dhl');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture default_store shipping/origin/country_id GB
     * @magentoConfigFixture default_store carriers/ups/type UPS_XML
     * @magentoConfigFixture default_store carriers/ups/active 1
     * @magentoConfigFixture default_store carriers/ups/shipper_number 12345
     * @magentoConfigFixture default_store carriers/ups/origin_shipment Shipments Originating in the European Union
     * @magentoConfigFixture default_store carriers/ups/username user
     * @magentoConfigFixture default_store carriers/ups/password pass
     * @magentoConfigFixture default_store carriers/ups/access_license_number acn
     * @magentoConfigFixture default_store currency/options/allow GBP,USD,EUR
     * @magentoConfigFixture default_store currency/options/base GBP
     */
    public function testGetRatesForActiveUpsCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('ups');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture default_store carriers/usps/allowed_methods 0_FCLE,0_FCL,0_FCP,1,2,3,4,6,7,13,16,17,22,23,25,27,28,33,34,35,36,37,42,43,53,55,56,57,61,INT_1,INT_2,INT_4,INT_6,INT_7,INT_8,INT_9,INT_10,INT_11,INT_12,INT_13,INT_14,INT_15,INT_16,INT_20,INT_26
     * @magentoConfigFixture default_store carriers/usps/showmethod 1
     * @magentoConfigFixture default_store carriers/usps/debug 1
     * @magentoConfigFixture default_store carriers/usps/userid test
     * @magentoConfigFixture default_store carriers/usps/mode 0
     * @magentoConfigFixture default_store carriers/usps/active 1
     * @magentoConfigFixture default_store carriers/usps/machinable true
     * @magentoConfigFixture default_store shipping/origin/country_id US
     * @magentoConfigFixture default_store shipping/origin/region_id
     * @magentoConfigFixture default_store shipping/origin/postcode 90034
     */
    public function testGetRatesForActiveUspsCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('usps');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
//        self::assertEquals(
//            [
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_15',
//                    28.25,
//                    'First-Class Package International Service',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_20',
//                    38.2,
//                    'Priority Mail International Small Flat Rate Envelope',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_8',
//                    38.2,
//                    'Priority Mail International Flat Rate Envelope',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_16',
//                    39.4,
//                    'Priority Mail International Small Flat Rate Box',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_2',
//                    57.25,
//                    'Priority Mail International',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_10',
//                    66.95,
//                    'Priority Mail Express International Flat Rate Envelope',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_1',
//                    71.3,
//                    'Priority Mail Express International',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_9',
//                    80.6,
//                    'Priority Mail International Medium Flat Rate Box',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_11',
//                    105.15,
//                    'Priority Mail International Large Flat Rate Box',
//                ),
//                new DefaultShippingRate(
//                    $shippingMethodId,
//                    'INT_12',
//                    106.05,
//                    'USPS GXG Envelopes',
//                ),
//            ],
//            $rates,
//        );
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/fedex/active 1
     */
    public function testGetRatesForActiveFedexCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('fedex');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/freeshipping/active 1
     */
    public function testGetRatesForActiveFreeShippingCarrierAndSimpleAvailableProductReturnsOneRate(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('freeshipping');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals(
            [
                new DefaultShippingRate(
                    $shippingMethodId,
                    'freeshipping',
                    0,
                    'Free',
                ),
            ],
            $rates,
        );

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/tablerate/active 1
     */
    public function testGetRatesForActiveTableRateCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('tablerate');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/pickup/active 1
     */
    public function testGetRatesForActivePickupCarrierAndSimpleAvailableProductReturnsNone(): void
    {
        // Arrange
        $shippingMethodId = new DefaultShippingMethodId('pickup');
        $simpleProduct = self::provideSimpleProduct();
        $productId = new DefaultProductId((string)$simpleProduct->getId());
        $shippingAddress = self::provideShippingAddress();

        // Act
        $rates = $this->priceCalculator->getRatesPerShippingMethod($shippingMethodId, $productId, $shippingAddress);

        // Assert
        self::assertEquals([], $rates);

        // Tear down
        $simpleProduct->rollback();
    }

    private static function provideSimpleProduct(): ProductFixture
    {
        return new ProductFixture(
            ProductBuilder::aSimpleProduct()
                ->withWeight(2)
                ->build()
        );
    }

    private static function provideShippingAddress(): Address
    {
        return AddressTestBuilder::create()
            ->setCountry('IE')
            ->setPostalCode('A94 VK61')
            ->setCity('Blackrock')
            ->setStreetAddress('13 Carysfort Grove, Stillorgan Park')
            ->build();
    }
}
