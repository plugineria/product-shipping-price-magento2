<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Magento2\Infrastructure\Service;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Domain\Model\DefaultProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\MagentoAvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Tests\Kit\Builder\AddressTestBuilder;
use TddWizard\Fixtures\Catalog\ProductBuilder;
use TddWizard\Fixtures\Catalog\ProductFixture;

class MagentoAvailableShippingMethodsResolverTest extends TestCase
{
    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/flatrate/active 0
     * @magentoConfigFixture default_store carriers/dhl/active 1
     * @magentoConfigFixture default_store carriers/dhl/showmethod 1
     * @magentoConfigFixture default_store carriers/ups/active 1
     * @magentoConfigFixture default_store carriers/usps/active 1
     * @magentoConfigFixture current_store carriers/fedex/active 1
     * @magentoConfigFixture current_store carriers/freeshipping/active 1
     * @magentoConfigFixture current_store carriers/tablerate/active 1
     */
    public function testWhenSaleableSimpleProductReturnListOfCarriers(): void
    {
        // Arrange
        /** @var MagentoAvailableShippingMethodsResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoAvailableShippingMethodsResolver::class);
        $productFixture = new ProductFixture(
            ProductBuilder::aSimpleProduct()->build()
        );
        $productId = new DefaultProductId((string)$productFixture->getId());
        $address = AddressTestBuilder::create()->build();

        // Act
        $shippingMethods = $resolver->getAvailableShippingMethods($productId, $address);

        // Assert
        self::assertEquals(
            [
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('dhl'),
                    'DHL',
                ),
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('fedex'),
                    'Federal Express',
                ),
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('tablerate'),
                    'Best Way',
                ),
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('freeshipping'),
                    'Free Shipping',
                ),
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('ups'),
                    'United Parcel Service',
                ),
                new DefaultShippingMethod(
                    new DefaultShippingMethodId('usps'),
                    'United States Postal Service',
                ),
            ],
            $shippingMethods
        );

        // Tear down
        $productFixture->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/flatrate/active 1
     */
    public function testWhenSaleableVirtualProductReturnNone(): void
    {
        // Arrange
        /** @var MagentoAvailableShippingMethodsResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoAvailableShippingMethodsResolver::class);
        $productFixture = new ProductFixture(
            ProductBuilder::aVirtualProduct()->build()
        );
        $productId = new DefaultProductId((string)$productFixture->getId());
        $address = AddressTestBuilder::create()->build();

        // Act
        $shippingMethods = $resolver->getAvailableShippingMethods($productId, $address);

        // Assert
        self::assertEquals([], $shippingMethods);

        // Tear down
        $productFixture->rollback();
    }

    /**
     * @magentoAppArea frontend
     *
     * @magentoConfigFixture current_store carriers/flatrate/active 1
     */
    public function testWhenDisabledSimpleProductReturnNone(): void
    {
        // Arrange
        /** @var MagentoAvailableShippingMethodsResolver $resolver */
        $resolver = Bootstrap::getObjectManager()->get(MagentoAvailableShippingMethodsResolver::class);
        $productFixture = new ProductFixture(
            ProductBuilder::aSimpleProduct()
                ->withStatus(Status::STATUS_DISABLED)
                ->build()
        );
        $productId = new DefaultProductId((string)$productFixture->getId());
        $address = AddressTestBuilder::create()->build();

        // Act
        $shippingMethods = $resolver->getAvailableShippingMethods($productId, $address);

        // Assert
        self::assertEquals([], $shippingMethods);

        // Tear down
        $productFixture->rollback();
    }
}
