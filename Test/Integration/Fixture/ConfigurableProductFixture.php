<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Integration\Fixture;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;

class ConfigurableProductFixture
{
    public const WEIGHT_BY_SKU = [
        'simple_10' => 2.20,
        'simple_20' => 3,
        'simple_30' => 300,
        'simple_40' => 301,
        'configurable_12345' => 4,
    ];

    public static function load(): void
    {
        require self::getBasePath() . 'Magento/ConfigurableProduct/_files/configurable_products.php';
        $productRepository = self::getProductRepository();
        foreach (self::WEIGHT_BY_SKU as $sku => $weight) {
            $product = $productRepository->get($sku);
            $product->setWeight($weight);
        }
    }

    private static function getProductRepository(): ProductRepositoryInterface
    {
        return Bootstrap::getObjectManager()->get(ProductRepositoryInterface::class);
    }

    public static function rollback(): void
    {
        require self::getBasePath() . 'Magento/ConfigurableProduct/_files/configurable_products_rollback.php';
    }

    private static function getBasePath(): string
    {
        return BP . '/dev/tests/integration/testsuite/';
    }
}
