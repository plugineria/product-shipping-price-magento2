<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Kit\App\Assembler;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use PHPUnit\Framework\Assert;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Magento2\App\Assembler\ProductIdBySkuAssemblerInterface;
use UnexpectedValueException;

class ProductIdBySkuAssembler extends Assert implements ProductIdBySkuAssemblerInterface
{
    /** @var string|null */
    private $expectedSku;

    /** @var ProductId|null */
    private $productId;

    public function getProductIdBySku(string $sku): ProductId
    {
        if (null === $this->expectedSku) {
            throw new UnexpectedValueException("Sku value is not defined");
        }

        self::assertEquals($this->expectedSku, $sku);

        if (null === $this->productId) {
            throw new NoSuchEntityException(new Phrase("Product not found for SKU {$sku}"));
        }

        return $this->productId;
    }

    public function setExpectedSku(string $sku): self
    {
        $this->expectedSku = $sku;

        return $this;
    }

    public function setProductId(?ProductId $productId): self
    {
        $this->productId = $productId;

        return $this;
    }
}
