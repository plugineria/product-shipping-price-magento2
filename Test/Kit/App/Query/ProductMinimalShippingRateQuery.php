<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Kit\App\Query;

use PHPUnit\Framework\Assert;
use Plugineria\ProductShippingPrice\App\Query\ProductMinimalShippingRateQueryPort;
use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use UnexpectedValueException;

class ProductMinimalShippingRateQuery extends Assert implements ProductMinimalShippingRateQueryPort
{
    /** @var ProductId|null */
    private $expectedProductId;

    /** @var ShippingRateView|null */
    private $shippingRateView;

    public function execute(ProductId $productId): ?ShippingRateView
    {
        if (null === $this->expectedProductId) {
            throw new UnexpectedValueException("ProductId is not defined");
        }

        return $this->shippingRateView;
    }

    public function setExpectedProductId(ProductId $productId): self
    {
        $this->expectedProductId = $productId;

        return $this;
    }

    public function setShippingRateView(?ShippingRateView $shippingRateView): self
    {
        $this->shippingRateView = $shippingRateView;

        return $this;
    }
}
