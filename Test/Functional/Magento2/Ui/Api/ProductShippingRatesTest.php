<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Functional\Magento2\Ui\Api;

use Magento\Framework\Webapi\Rest\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;

class ProductShippingRatesTest extends WebapiAbstract
{
    public function testWhenSkuAndRatesExistReturnRateInfo(): void
    {
        // Arrange
        $sku = 'MJ06';
        $serviceInfo = [
            'rest' => [
                'resourcePath' => "/V1/products/$sku/shippingRates",
                'httpMethod' => Request::HTTP_METHOD_GET,
            ],
        ];

        // Act
        $result = $this->_webApiCall($serviceInfo);

        // Assert
        self::assertEquals(
            [
                [
                    'price' => 5,
                    'code' => 'flatrate',
                    'title' => 'Fixed',
                    'shipping_method' => [
                        'id' => 'flatrate',
                        'title' => 'Flat Rate',
                    ]
                ],
                [
                    'price' => 15,
                    'code' => 'bestway',
                    'title' => 'Table Rate',
                    'shipping_method' => [
                        'id' => 'tablerate',
                        'title' => 'Best Way',
                    ]
                ],
            ],
            $result
        );
    }

    public function testWhenSkuExistAndProductIsVirtualReturnRateEmptyList(): void
    {
        // Arrange
        $sku = '240-LV04';
        $serviceInfo = [
            'rest' => [
                'resourcePath' => "/V1/products/$sku/shippingRates",
                'httpMethod' => Request::HTTP_METHOD_GET,
            ],
        ];

        // Act
        $result = $this->_webApiCall($serviceInfo);

        // Assert
        self::assertEquals([], $result);
    }
}
