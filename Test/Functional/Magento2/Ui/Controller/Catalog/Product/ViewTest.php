<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Functional\Magento2\Ui\Controller\Catalog\Product;

use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Test\Functional\Page\Catalog\Product\ViewPage;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class ViewTest extends TestCase
{
    /** @var HttpClient */
    private $client;

    /** @var string */
    private $baseUrl;

    protected function setUp(): void
    {
        $this->baseUrl = $_ENV['MAGENTO_URL'];
        $this->client = HttpClient::createForBaseUri($this->baseUrl);
    }

    public function testViewShippableProductShowsMinimalShippingRateAndLinkToListOfRates(): void
    {
        // Act
        $response = $this->client->request('GET', '/catalog/product/view/id/404', [
            'timeout' => 120,
        ]);

        // Assert
        self::assertEquals(200, $response->getStatusCode());

        $crawler = new Crawler($response->getContent());
        self::assertEquals(
            'Shipping from €5.00',
            $crawler->filter(ViewPage::MINIMAL_SHIPPING_RATE_TEXT)->text(null, true)
        );
        self::assertEquals(
            'Shipping',
            $crawler->filter(ViewPage::ALL_SHIPPING_RATES_LINK)->text(null, true)
        );
        self::assertEquals(
            $this->baseUrl . '/index.php/catalog/product/shippingRates/id/404/',
            $crawler->filter(ViewPage::ALL_SHIPPING_RATES_LINK)->attr('href')
        );
    }
}
