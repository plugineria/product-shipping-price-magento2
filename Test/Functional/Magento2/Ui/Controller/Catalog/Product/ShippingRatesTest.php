<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Functional\Magento2\Ui\Controller\Catalog\Product;

use PHPUnit\Framework\TestCase;
use Plugineria\ProductShippingPrice\Test\Functional\Page\Catalog\Product\ShippingRatesPage;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class ShippingRatesTest extends TestCase
{
    /** @var HttpClient */
    private $client;

    /** @var string */
    private $baseUrl;

    protected function setUp(): void
    {
        $this->baseUrl = $_ENV['MAGENTO_URL'];
        $this->client = HttpClient::createForBaseUri($this->baseUrl);
    }

    public function testViewShippableProductShowsShippingAddressAndListOfRates(): void
    {
        // Act
        $response = $this->client->request('GET', '/catalog/product/shippingRates/id/404');

        // Assert
        self::assertEquals(200, $response->getStatusCode());

        $crawler = new Crawler($response->getContent());
        self::assertEquals(
            '90034, California',
            $crawler->filter(ShippingRatesPage::SESSION_SHIPPING_ADDRESS)->text(null, true)
        );
        self::assertEquals(
            'Flat Rate / Fixed',
            $crawler->filter(ShippingRatesPage::FLATRATE_TITLE)->text(null, true)
        );
        self::assertEquals(
            '€5.00',
            $crawler->filter(ShippingRatesPage::FLATRATE_PRICE)->text(null, true)
        );
        self::assertEquals(
            'Best Way / Table Rate',
            $crawler->filter(ShippingRatesPage::TABLERATE_TITLE)->text(null, true)
        );
        self::assertEquals(
            '€15.00',
            $crawler->filter(ShippingRatesPage::TABLERATE_PRICE)->text(null, true)
        );
    }
}
