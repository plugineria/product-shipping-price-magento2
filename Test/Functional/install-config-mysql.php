<?php

declare(strict_types=1);

return [
    'language' => $_ENV['MAGENTO_LANGUAGE'],
    'timezone' => $_ENV['MAGENTO_TIMEZONE'],
    'currency' => $_ENV['MAGENTO_DEFAULT_CURRENCY'],
    'db-host' => $_ENV['MYSQL_HOST'],
    'db-user' => $_ENV['MYSQL_USER'],
    'db-password' => $_ENV['MYSQL_PASSWORD'],
    'db-name' => $_ENV['MYSQL_DATABASE'],
    'backend-frontname' => $_ENV['MAGENTO_BACKEND_FRONTNAME'],
    'base-url' => $_ENV['MAGENTO_URL'],
    'use-secure' => '0',
    'use-rewrites' => '0',
    'admin-lastname' => $_ENV['MAGENTO_ADMIN_FIRSTNAME'],
    'admin-firstname' => $_ENV['MAGENTO_ADMIN_LASTNAME'],
    'admin-email' => $_ENV['MAGENTO_ADMIN_EMAIL'],
    'admin-user' => $_ENV['MAGENTO_ADMIN_USERNAME'],
    'admin-password' => $_ENV['MAGENTO_ADMIN_PASSWORD'],
    'admin-use-security-key' => '0',
    /* PayPal has limitation for order number - 20 characters. 10 digits prefix + 8 digits number is good enough */
    'sales-order-increment-prefix' => time(),
    'session-save' => 'db',
    'cleanup-database' => false,
];
