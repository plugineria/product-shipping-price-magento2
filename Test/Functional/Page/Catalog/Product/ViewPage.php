<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Functional\Page\Catalog\Product;

class ViewPage
{
    public const MINIMAL_SHIPPING_RATE_TEXT =
        '#maincontent .product-info-main .product-minimal-shipping-rate .minimal-rate-info';
    public const ALL_SHIPPING_RATES_LINK =
        '#maincontent .product-info-main .product-minimal-shipping-rate a.all-rates';
}
