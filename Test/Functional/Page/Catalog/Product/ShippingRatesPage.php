<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Test\Functional\Page\Catalog\Product;

class ShippingRatesPage
{
    public const SESSION_SHIPPING_ADDRESS =
        '#maincontent .session-shipping-address-container #session-shipping-address';

    public const FLATRATE_TITLE = '#maincontent .product-shipping-rates .rate-code-flatrate .rate-title';
    public const FLATRATE_PRICE = '#maincontent .product-shipping-rates .rate-code-flatrate .rate-price .price';
    public const TABLERATE_TITLE = '#maincontent .product-shipping-rates .rate-code-bestway .rate-title';
    public const TABLERATE_PRICE = '#maincontent .product-shipping-rates .rate-code-bestway .rate-price .price';
}
