# Product Shipping Price for Magento2 

Estimate product shipping rates right on product page. 

* Calculates minimal available shipping rate on product page.
* Displays all available shipping options for signle product before adding to cart.
* Provides REST API endpoints
* Possibility to configure default shipping address or use existing address for registered users.

## Tested shipping methods

* Flatrate
* DHL
* UPS
* US Postal Service
* Federal Express
* Free Shipping
* Table rate

## Tested product types

* Simple
* Virtual
* Configurable

# Architecture

An implementation of [Magento2](https://magento.com/) module with [Hexagonal architecture](https://alistair.cockburn.us/hexagonal-architecture/) by [Yury Ksenevich](https://www.spadar.com/).

Source code is split into 4 tiers: 

1. Domain / Business logic - implemented in [`plugineria/product-shipping-price`](https://gitlab.com/plugineria/product-shipping-price) library.
2. Application logic - implemented in [`plugineria/product-shipping-price`](https://gitlab.com/plugineria/product-shipping-price) library.
3. Infrastructure - Magento-specific adapters for business logic, located in `Magento2/Infrastructure/`
4. UI - API endpoints, frontend controllers and routers located in `Magento2/Ui/`


# System requirements

* Tested on Magento versions: 2.3.6+, 2.4.1+
* PHP 7.3 or 7.4


# Installation as a Magento2 module

:warning: Please note that you can only install the extension using composer.

1. Backup your store database and web directory
2. Open a terminal and move to Magento root directory

3. run these commands in console:

```bash
composer require plugineria/product-shipping-price-magento2
bin/magento module:enable Plugineria_ProductShippingPrice
```

4. Configure Example Shipping Address in `admin` > `Stores` > `Configuration` > `Plugineria` > `Product Shipping Price` > `Example Shipping Address`

5. Flush cache

# REST API

## GET /V1/products/{sku}/shippingRates

Gets list of shipping rates for product

### Request params
* sku - string product SKU

### Response

```json
[
  {
    "price": 0,
    "code": "string",
    "title": "string",
    "description": "string",
    "shipping_method": {
      "id": "string",
      "title": "string"
    }
  }
]
```

## GET /V1/products/{sku}/shippingRates/minimal

Gets minimal shipping rate for product

### Request params
* sku - string product SKU

### Response

If rate found:

```json
{
  "price": 0,
  "code": "string",
  "title": "string",
  "description": "string",
  "shipping_method": {
    "id": "string",
    "title": "string"
  }
}
```

If rate not found:

```json
{}
```

# User Interface

## Product page

Minimal shipping rate block in add to cart section:

![Minimal estimated shipping rate](docs/images/product-shipping-minimal.png)

Popup with all shipping rates for product:

![All shipping rates for product](docs/images/product-shipping-list.png)


# License

* [OSL 3.0](https://opensource.org/licenses/OSL-3.0) 
* [AFL 3.0](https://opensource.org/licenses/AFL-3.0)
* Icons by [Semantic UI](https://semantic-ui.com/)
* No warranty, explicit or implicit, provided.


## Contributing

By contributing to this project, you grant a world-wide, royalty-free, perpetual, irrevocable, non-exclusive, transferable license to all users under the terms of the license(s) under which this project is distributed.