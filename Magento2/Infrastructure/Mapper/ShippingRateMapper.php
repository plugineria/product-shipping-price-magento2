<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper;

use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\DefaultShippingRate;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;

class ShippingRateMapper
{
    private const MAGE_PRICE = 'price';
    private const MAGE_CODE = 'method';
    private const MAGE_TITLE = 'method_title';

    public function mageToDomain(ShippingMethodId $methodId, Method $shippingRate): ShippingRate
    {
        return new DefaultShippingRate(
            $methodId,
            $shippingRate->getData(self::MAGE_CODE),
            (float)$shippingRate->getData(self::MAGE_PRICE),
            $shippingRate->getData(self::MAGE_TITLE),
        );
    }
}
