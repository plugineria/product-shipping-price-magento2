<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper;

use Magento\Shipping\Model\Carrier\AbstractCarrierInterface;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\DefaultShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;

class ShippingMethodMapper
{
    public function mageToDomain(AbstractCarrierInterface $carrier): ShippingMethod
    {
        return new DefaultShippingMethod(
            new DefaultShippingMethodId($carrier->getCarrierCode()),
            $carrier->getConfigData('title')
        );
    }
}
