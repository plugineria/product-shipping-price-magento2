<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper;

use Magento\Customer\Api\Data\AddressInterface;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\DefaultCustomerShippingAddress;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoCustomerId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoCustomerShippingAddressId;

use function implode;

class CustomerShippingAddressMapper
{
    public function mageToDomain(AddressInterface $address): DefaultCustomerShippingAddress
    {
        return new DefaultCustomerShippingAddress(
            new MagentoCustomerShippingAddressId((int)$address->getId()),
            new DefaultAddress(
                $address->getCountryId(),
                $address->getCity(),
                $address->getPostcode(),
                implode(', ', $address->getStreet()),
                $address->getRegion()->getRegion()
            ),
            new MagentoCustomerId((int)$address->getCustomerId()),
        );
    }
}
