<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Repository;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Framework\Exception\LocalizedException;
use Plugineria\ProductShippingPrice\Domain\Exception\CustomerShippingAddressNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddressId;
use Plugineria\ProductShippingPrice\Domain\Repository\CustomerShippingAddressRepository;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper\CustomerShippingAddressMapper;

class MagentoCustomerShippingAddressRepository implements CustomerShippingAddressRepository
{
    /** @var AddressRepositoryInterface */
    private $customerAddressRepository;

    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    /** @var CustomerShippingAddressMapper */
    private $customerShippingAddressMapper;

    public function __construct(
        AddressRepositoryInterface $customerAddressRepository,
        CustomerRepositoryInterface $customerRepository,
        CustomerShippingAddressMapper $customerShippingAddressMapper
    ) {
        $this->customerAddressRepository = $customerAddressRepository;
        $this->customerRepository = $customerRepository;
        $this->customerShippingAddressMapper = $customerShippingAddressMapper;
    }

    public function findById(CustomerShippingAddressId $id): ?CustomerShippingAddress
    {
        try {
            $address = $this->getById($id);
        } catch (CustomerShippingAddressNotFound $e) {
            return null;
        }

        return $address;
    }

    public function getById(CustomerShippingAddressId $id): CustomerShippingAddress
    {
        try {
            $address = $this->customerAddressRepository->getById((int)(string)$id);
        } catch (LocalizedException $e) {
            throw new CustomerShippingAddressNotFound($id);
        }

        return $this->customerShippingAddressMapper->mageToDomain($address);
    }

    public function findAllByCustomerId(CustomerId $customerId): array
    {
        $customer = $this->customerRepository->getById((int)(string)$customerId);
        $addresses = $customer->getAddresses();

        if (null === $addresses) {
            return [];
        }

        usort(
            $addresses,
            static function (AddressInterface $address1, AddressInterface $address2): int {
                if ($address1->isDefaultShipping()) {
                    return -1;
                }

                if ($address2->isDefaultShipping()) {
                    return 1;
                }

                return 0;
            }
        );

        return array_map(
            function (AddressInterface $address): CustomerShippingAddress {
                return $this->customerShippingAddressMapper->mageToDomain($address);
            },
            $addresses
        );
    }
}
