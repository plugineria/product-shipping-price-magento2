<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Repository;

use Magento\Catalog\Model\Session;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use Plugineria\ProductShippingPrice\Domain\Repository\SessionShippingAddressRepository;

use function explode;
use function implode;
use function ucfirst;

class MagentoSessionShippingAddressRepository implements SessionShippingAddressRepository
{
    private const SESSION_KEY = 'plugineria_session_shipping_address';

    /** @var Session */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function save(Address $address): void
    {
        $this->session->{$this->getSessionSetterMethod()}((string)$address);
    }

    public function get(): ?Address
    {
        $serializedAddress = $this->session->getData(self::SESSION_KEY);

        return $serializedAddress ? DefaultAddress::createFromJson($serializedAddress) : null;
    }

    private function getSessionSetterMethod(): string
    {
        return 'set' . implode(
            '',
            array_map(
                static function (string $word): string {
                    return ucfirst($word);
                },
                explode('_', self::SESSION_KEY)
            )
        );
    }
}
