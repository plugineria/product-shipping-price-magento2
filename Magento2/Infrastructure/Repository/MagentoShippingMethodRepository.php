<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Repository;

use Magento\Shipping\Model\CarrierFactoryInterface;
use Plugineria\ProductShippingPrice\Domain\Exception\ShippingMethodNotFound;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Repository\ShippingMethodRepository;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper\ShippingMethodMapper;

class MagentoShippingMethodRepository implements ShippingMethodRepository
{
    /** @var CarrierFactoryInterface */
    private $carrierFactory;

    /** @var ShippingMethodMapper */
    private $shippingMethodMapper;

    public function __construct(CarrierFactoryInterface $carrierFactory, ShippingMethodMapper $shippingMethodMapper)
    {
        $this->carrierFactory = $carrierFactory;
        $this->shippingMethodMapper = $shippingMethodMapper;
    }

    public function getById(ShippingMethodId $shippingMethodId): ShippingMethod
    {
        $shippingMethod = $this->findById($shippingMethodId);

        if (null === $shippingMethod) {
            throw new ShippingMethodNotFound($shippingMethodId);
        }

        return $shippingMethod;
    }

    public function findById(ShippingMethodId $shippingMethodId): ?ShippingMethod
    {
        return $this->shippingMethodMapper->mageToDomain(
            $this->carrierFactory->get((string)$shippingMethodId)
        );
    }
}
