<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Assembler;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Magento2\App\Assembler\ProductIdBySkuAssemblerInterface;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;

class ProductIdBySkuAssembler implements ProductIdBySkuAssemblerInterface
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProductIdBySku(string $sku): ProductId
    {
        $product = $this->productRepository->get($sku);

        return new MagentoProductId((int)$product->getId());
    }
}
