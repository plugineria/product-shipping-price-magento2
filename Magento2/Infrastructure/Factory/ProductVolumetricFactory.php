<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Factory;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Volumetric;

class ProductVolumetricFactory
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * If parent product has weight assume it has all volumetric info, otherwise get info from first child.
     */
    public function create(MagentoProductId $productId): Volumetric
    {
        /** @var Product $product */
        $product = $this->productRepository->getById($productId->getId());
        $weight = $product->getWeight();

        if ($weight) {
            return new Volumetric((float)$weight);
        }

        $childProductIds = $product->getTypeInstance()->getChildrenIds($productId->getId());

        if (array_key_exists(0, $childProductIds)) {
            foreach ($childProductIds[0] as $childProductId) {
                return $this->create(new MagentoProductId((int)$childProductId));
            }
        }

        return new Volumetric($weight ?? null);
    }
}
