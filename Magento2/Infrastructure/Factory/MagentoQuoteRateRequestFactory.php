<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Factory;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Directory\Model\RegionFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Config\OriginAddressConfig;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote\ItemFactory;

class MagentoQuoteRateRequestFactory
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var OriginAddressConfig */
    private $originAddressConfig;

    /** @var ItemFactory */
    private $quoteItemFactory;

    /** @var RegionFactory */
    private $regionFactory;

    /** @var ProductVolumetricFactory */
    private $productVolumetricFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        OriginAddressConfig $originAddressConfig,
        ItemFactory $quoteItemFactory,
        RegionFactory $regionFactory,
        ProductVolumetricFactory $productVolumetricFactory
    ) {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->originAddressConfig = $originAddressConfig;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->regionFactory = $regionFactory;
        $this->productVolumetricFactory = $productVolumetricFactory;
    }

    /**
     * @see \Magento\Quote\Model\Quote\Address::requestShippingRates()
     */
    public function create(MagentoProductId $productId, Address $shippingAddress): RateRequest
    {
        $rateRequest = new RateRequest();

        /** @var Product $product */
        $product = $this->productRepository->getById($productId->getId());
        $volumetric = $this->productVolumetricFactory->create($productId);

        /** @var Store $currentStore */
        $currentStore = $this->storeManager->getStore();

        $rateRequest
            ->setAllItems([
                $this->quoteItemFactory->create($productId, $shippingAddress)
            ])

            ->setOrigCountryId($this->originAddressConfig->getCountry())
            ->setOrigRegionId($this->originAddressConfig->getRegionId())
            ->setOrigPostcode($this->originAddressConfig->getPostalCode())
            ->setOrigCity($this->originAddressConfig->getCity())

            ->setDestCountryId($shippingAddress->getCountry())
            ->setDestCity($shippingAddress->getCity())
            ->setDestPostcode($shippingAddress->getPostalCode())
            ->setDestStreet($shippingAddress->getStreetAddress())

            ->setFreeMethodWeight(0)
            ->setStoreId($currentStore->getId())
            ->setWebsiteId($currentStore->getWebsiteId())
            ->setBaseCurrency($currentStore->getBaseCurrencyCode())

            ->setPackageQty(1)
            ->setPackagePhysicalValue(1)
            ->setPackageCurrency($currentStore->getCurrentCurrencyCode())

            ->setPackageValue($product->getPrice())
            ->setPackageValueWithDiscount($product->getPrice())
            ->setPackageWeight($volumetric->getWeight())
        ;

        if ($shippingAddress->getRegion()) {
            $region = $this->regionFactory->create()->loadByName(
                $shippingAddress->getRegion(),
                $shippingAddress->getCountry()
            );

            $rateRequest
                ->setDestRegionCode($region->getCode())
                ->setDestRegionId($region->getId());
        }

        return $rateRequest;
    }
}
