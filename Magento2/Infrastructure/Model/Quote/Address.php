<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\RegionInterface;
use Magento\Framework\Api\AttributeInterface;
use Magento\Quote\Api\Data\AddressExtensionInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address as ShippingAddress;

class Address implements AddressInterface
{
    /** @var ShippingAddress */
    private $address;

    /** @var RegionInterface|null */
    private $region;

    /** @var CustomerInterface|null */
    private $customer;

    /** @var Quote */
    private $quote;

    public function __construct(
        ShippingAddress $address,
        ?RegionInterface $region,
        ?CustomerInterface $customer,
        Quote $quote
    ) {
        $this->address = $address;
        $this->region = $region;
        $this->customer = $customer;
        $this->quote = $quote;
    }

    public function getId(): ?int
    {
        return null;
    }

    public function setId($id): self
    {
        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region ? $this->region->getRegion() : null;
    }

    public function setRegion($region): self
    {
        return $this;
    }

    public function getRegionId(): ?int
    {
        return $this->region ? $this->region->getRegionId() : null;
    }

    public function setRegionId($regionId): self
    {
        return $this;
    }

    public function getRegionCode(): ?string
    {
        return $this->region ? $this->region->getRegionCode() : null;
    }

    public function setRegionCode($regionCode): self
    {
        return $this;
    }

    public function getCountryId(): string
    {
        return $this->address->getCountry();
    }

    public function setCountryId($countryId): self
    {
        return $this;
    }

    public function getStreet(): string
    {
        return (string)$this->address->getStreetAddress();
    }

    public function setStreet($street): self
    {
        return $this;
    }

    public function getCompany(): ?string
    {
        return null;
    }

    public function setCompany($company): self
    {
        return $this;
    }

    public function getTelephone(): string
    {
        return '';
    }

    public function setTelephone($telephone): self
    {
        return $this;
    }

    public function getFax(): ?string
    {
        return null;
    }

    public function setFax($fax): self
    {
        return $this;
    }

    public function getPostcode(): string
    {
        return (string)$this->address->getPostalCode();
    }

    public function setPostcode($postcode): self
    {
        return $this;
    }

    public function getCity(): string
    {
        return $this->address->getCity();
    }

    public function setCity($city): self
    {
        return $this;
    }

    public function getFirstname(): string
    {
        return '';
    }

    public function setFirstname($firstname): self
    {
        return $this;
    }

    public function getLastname(): string
    {
        return '';
    }

    public function setLastname($lastname): self
    {
        return $this;
    }

    public function getMiddlename(): ?string
    {
        return null;
    }

    public function setMiddlename($middlename): self
    {
        return $this;
    }

    public function getPrefix(): ?string
    {
        return null;
    }

    public function setPrefix($prefix): self
    {
        return $this;
    }

    public function getSuffix(): ?string
    {
        return null;
    }

    public function setSuffix($suffix): self
    {
        return $this;
    }

    public function getVatId(): ?string
    {
        return null;
    }

    public function setVatId($vatId): self
    {
        return $this;
    }

    public function getCustomerId(): ?int
    {
        return $this->customer ? $this->customer->getId() : null;
    }

    public function setCustomerId($customerId): self
    {
        return $this;
    }

    public function getEmail(): string
    {
        return '';
    }

    public function setEmail($email): self
    {
        return $this;
    }

    public function getSameAsBilling(): ?int
    {
        return 1;
    }

    public function setSameAsBilling($sameAsBilling): self
    {
        return $this;
    }

    public function getCustomerAddressId(): ?int
    {
        return null;
    }

    public function setCustomerAddressId($customerAddressId): self
    {
        return $this;
    }

    public function getSaveInAddressBook(): ?int
    {
        return null;
    }

    public function setSaveInAddressBook($saveInAddressBook): self
    {
        return $this;
    }

    public function getExtensionAttributes(): ?AddressExtensionInterface
    {
        return null;
    }

    public function setExtensionAttributes(AddressExtensionInterface $extensionAttributes): self
    {
        return $this;
    }

    public function getCustomAttributes(): ?array
    {
        return [];
    }

    public function setCustomAttributes(array $attributes): self
    {
        return $this;
    }

    public function getCustomAttribute($attributeCode): ?AttributeInterface
    {
        return null;
    }

    public function setCustomAttribute($attributeCode, $attributeValue): self
    {
        return $this;
    }

    public function getQuote(): Quote
    {
        return $this->quote;
    }

    public function getFreeShipping(): bool
    {
        return false;
    }
}
