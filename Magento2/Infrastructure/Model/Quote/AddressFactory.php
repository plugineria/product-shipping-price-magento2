<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Directory\Model\RegionFactory;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address as ShippingAddress;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\MagentoCustomerResolver;

class AddressFactory
{
    /** @var MagentoCustomerResolver */
    private $customerResolver;

    /** @var RegionFactory */
    private $regionFactory;

    public function __construct(MagentoCustomerResolver $customerResolver, RegionFactory $regionFactory)
    {
        $this->customerResolver = $customerResolver;
        $this->regionFactory = $regionFactory;
    }

    public function create(ShippingAddress $address, Quote $quote): Address
    {
        return new Address(
            $address,
            $this->getRegion($address),
            $this->customerResolver->getCurrentCustomer(),
            $quote
        );
    }

    private function getRegion(ShippingAddress $address): ?Region
    {
        $region = $this->regionFactory->create()->loadByCode($address->getRegion(), $address->getCountry());

        if (null === $region->getId()) {
            return null;
        }

        return new Region(
            $region->getId(),
            $region->getCode(),
            $region->getName(),
        );
    }
}
