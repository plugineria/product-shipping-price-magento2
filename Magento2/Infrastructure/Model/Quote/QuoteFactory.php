<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\MagentoCustomerResolver;

class QuoteFactory
{
    /** @var MagentoCustomerResolver */
    private $customerResolver;

    /** @var StoreManagerInterface */
    private $storeManager;

    public function __construct(MagentoCustomerResolver $customerResolver, StoreManagerInterface $storeManager)
    {
        $this->customerResolver = $customerResolver;
        $this->storeManager = $storeManager;
    }

    public function create(): Quote
    {
        /** @var Store $currentStore */
        $currentStore = $this->storeManager->getStore();

        return new Quote(
            $this->customerResolver->getCurrentCustomer(),
            $this->getQuoteCurrency($currentStore),
            (int)$currentStore->getId()
        );
    }

    private function getQuoteCurrency(Store $store): Currency
    {
        return new Currency(
            $store->getBaseCurrencyCode(),
            $store->getCurrentCurrencyCode(),
            (float)$store->getCurrentCurrencyRate(),
        );
    }
}
