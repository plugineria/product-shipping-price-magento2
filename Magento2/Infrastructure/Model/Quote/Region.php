<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Customer\Api\Data\RegionExtensionInterface;
use Magento\Customer\Api\Data\RegionInterface;

class Region implements RegionInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $code;

    /** @var string */
    private $name;

    public function __construct(int $id, string $code, string $name)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * Get region code
     *
     * @return string
     */
    public function getRegionCode(): string
    {
        return $this->code;
    }

    public function setRegionCode($regionCode): self
    {
        return $this;
    }

    public function getRegion(): string
    {
        return $this->name;
    }

    public function setRegion($region): self
    {
        return $this;
    }

    public function getRegionId(): int
    {
        return $this->id;
    }

    public function setRegionId($regionId): self
    {
        return $this;
    }

    public function getExtensionAttributes(): ?RegionExtensionInterface
    {
        return null;
    }

    public function setExtensionAttributes(RegionExtensionInterface $extensionAttributes): self
    {
        return $this;
    }
}
