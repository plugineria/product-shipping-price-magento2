<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\CartExtensionInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CurrencyInterface;

class Quote implements CartInterface
{
    /** @var CustomerInterface|null */
    private $customer;

    /** @var CurrencyInterface */
    private $currency;

    /** @var int */
    private $storeId;

    public function __construct(?CustomerInterface $customer, CurrencyInterface $currency, int $storeId)
    {
        $this->customer = $customer;
        $this->currency = $currency;
        $this->storeId = $storeId;
    }

    public function getId(): ?int
    {
        return null;
    }

    public function setId($id): self
    {
        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return null;
    }

    public function setCreatedAt($createdAt): self
    {
        return $this;
    }

    public function getUpdatedAt(): ?string
    {
        return null;
    }

    public function setUpdatedAt($updatedAt): self
    {
        return $this;
    }

    public function getConvertedAt(): ?string
    {
        return null;
    }

    public function setConvertedAt($convertedAt): self
    {
        return $this;
    }

    public function getIsActive(): ?bool
    {
        return true;
    }

    public function setIsActive($isActive): self
    {
        return $this;
    }

    public function getIsVirtual(): ?bool
    {
        return false;
    }

    public function getItems(): ?array
    {
        return [];
    }

    public function setItems(array $items = null): self
    {
        return $this;
    }

    public function getItemsCount(): ?int
    {
        return 1;
    }

    public function setItemsCount($itemsCount): self
    {
        return $this;
    }

    public function getItemsQty(): ?float
    {
        return 1;
    }

    public function setItemsQty($itemQty): self
    {
        return $this;
    }

    public function getCustomer(): ?CustomerInterface
    {
        return $this->customer;
    }

    public function setCustomer(CustomerInterface $customer = null): self
    {
        return $this;
    }

    public function getBillingAddress(): ?AddressInterface
    {
        return null;
    }

    public function setBillingAddress(AddressInterface $billingAddress = null): self
    {
        return $this;
    }

    public function getReservedOrderId(): ?string
    {
        return null;
    }

    public function setReservedOrderId($reservedOrderId): self
    {
        return $this;
    }

    public function getOrigOrderId(): ?int
    {
        return null;
    }

    public function setOrigOrderId($origOrderId): self
    {
        return $this;
    }

    public function getCurrency(): ?CurrencyInterface
    {
        return $this->currency;
    }

    public function setCurrency(CurrencyInterface $currency = null): self
    {
        return $this;
    }

    public function getCustomerIsGuest(): ?bool
    {
        return null === $this->customer;
    }

    public function setCustomerIsGuest($customerIsGuest): self
    {
        return $this;
    }

    public function getCustomerNote(): ?string
    {
        return null;
    }

    public function setCustomerNote($customerNote): self
    {
        return $this;
    }

    public function getCustomerNoteNotify(): ?bool
    {
        return false;
    }

    public function setCustomerNoteNotify($customerNoteNotify): self
    {
        return $this;
    }

    public function getCustomerTaxClassId(): ?int
    {
        if (null === $this->customer) {
            return null;
        }

        $taxClassId = $this->customer->getTaxClassId();

        return $taxClassId ? (int)$taxClassId : null;
    }

    public function setCustomerTaxClassId($customerTaxClassId): self
    {
        return $this;
    }

    /**
     * Get store identifier
     *
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->storeId;
    }

    public function setStoreId($storeId): self
    {
        return $this;
    }

    public function getExtensionAttributes(): ?CartExtensionInterface
    {
        return null;
    }

    public function setExtensionAttributes(CartExtensionInterface $extensionAttributes): self
    {
        return $this;
    }
}
