<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Quote\Api\Data\CurrencyExtensionInterface;
use Magento\Quote\Api\Data\CurrencyInterface;

class Currency implements CurrencyInterface
{
    /** @var string */
    private $baseCurrencyCode;

    /** @var string */
    private $currentCurrencyCode;

    /** @var float */
    private $currentToBaseRate;

    public function __construct(string $baseCurrencyCode, string $currentCurrencyCode, float $currentToBaseRate)
    {
        $this->baseCurrencyCode = $baseCurrencyCode;
        $this->currentCurrencyCode = $currentCurrencyCode;
        $this->currentToBaseRate = $currentToBaseRate;
    }

    public function getGlobalCurrencyCode(): ?string
    {
        return $this->baseCurrencyCode;
    }

    public function setGlobalCurrencyCode($globalCurrencyCode): self
    {
        return $this;
    }

    public function getBaseCurrencyCode(): ?string
    {
        return $this->baseCurrencyCode;
    }

    public function setBaseCurrencyCode($baseCurrencyCode): self
    {
        return $this;
    }

    /**
     * Get store currency code
     *
     * @return string|null
     */
    public function getStoreCurrencyCode(): ?string
    {
        return $this->baseCurrencyCode;
    }

    public function setStoreCurrencyCode($storeCurrencyCode): self
    {
        return $this;
    }

    public function getQuoteCurrencyCode(): ?string
    {
        return $this->currentCurrencyCode;
    }

    public function setQuoteCurrencyCode($quoteCurrencyCode): self
    {
        return $this;
    }

    public function getStoreToBaseRate(): ?float
    {
        return 1;
    }

    public function setStoreToBaseRate($storeToBaseRate): self
    {
        return $this;
    }

    public function getStoreToQuoteRate(): ?float
    {
        return $this->currentToBaseRate;
    }

    public function setStoreToQuoteRate($storeToQuoteRate): self
    {
        return $this;
    }

    public function getBaseToGlobalRate(): ?float
    {
        return 1;
    }

    public function setBaseToGlobalRate($baseToGlobalRate): self
    {
        return $this;
    }

    public function getBaseToQuoteRate(): ?float
    {
        return $this->currentToBaseRate;
    }

    public function setBaseToQuoteRate($baseToQuoteRate): self
    {
        return $this;
    }

    public function getExtensionAttributes(): ?CurrencyExtensionInterface
    {
        return null;
    }

    public function setExtensionAttributes(CurrencyExtensionInterface $extensionAttributes): self
    {
        return $this;
    }
}
