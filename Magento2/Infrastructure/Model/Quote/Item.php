<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\Framework\DataObject;
use Magento\Quote\Api\Data\CartItemExtensionInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Api\Data\ProductOptionInterface;
use Magento\Quote\Model\Quote\Item\Option;

/**
 * @see \Magento\Quote\Model\Quote\Item\AbstractItem
 */
class Item implements ItemInterface, CartItemInterface
{
    /** @var Product */
    private $product;

    /** @var Option[] */
    private $options;

    /** @var Address */
    private $address;

    /** @var Quote */
    private $quote;

    public function __construct(Product $product, Address $address, Quote $quote, Option ...$options)
    {
        $this->product = $product;
        $this->address = $address;
        $this->quote = $quote;
        $this->options = $options;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getOptionByCode($code): ?Product\Configuration\Item\Option\OptionInterface
    {
        foreach ($this->options as $option) {
            if ($option->getCode() === $code) {
                return $option;
            }
        }

        return null;
    }

    public function getFileDownloadParams(): ?DataObject
    {
        return null;
    }

    public function getParentItem(): ?self
    {
        return null;
    }

    public function getHasChildren(): bool
    {
        return false;
    }

    /**
     * @TODO implement free shipping
     */
    public function getFreeShipping(): bool
    {
        return false;
    }

    /**
     * @return Item[]
     */
    public function getChildren(): array
    {
        return [];
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getQuote(): Quote
    {
        return $this->quote;
    }

    public function getQty(): int
    {
        return 1;
    }

    public function setQty($qty): self
    {
        return $this;
    }

    public function getTotalQty(): int
    {
        return 1;
    }

    public function calcRowTotal(): float
    {
        return $this->getPrice();
    }

    public function getCalculationPrice(): float
    {
        return $this->getPrice();
    }

    public function getCalculationPriceOriginal(): float
    {
        return $this->getPrice();
    }

    public function getBaseCalculationPriceOriginal(): float
    {
        return $this->getPrice();
    }

    public function getOriginalPrice(): float
    {
        return $this->getPrice();
    }

    public function getBaseOriginalPrice(): float
    {
        return $this->getPrice();
    }

    public function getPrice(): float
    {
        return $this->getProduct()->getPrice();
    }

    public function setPrice($price): self
    {
        return $this;
    }

    public function getConvertedPrice(): float
    {
        return $this->getPrice();
    }

    public function isChildrenCalculated(): bool
    {
        return true;
    }

    public function isShipSeparately(): bool
    {
        return false;
    }

    public function getTotalDiscountAmount(): float
    {
        return 0;
    }

    public function getProductType(): string
    {
        return $this->product->getTypeId();
    }

    public function setProductType($productType)
    {
        return $this;
    }

    public function getName(): string
    {
        return $this->product->getName();
    }

    public function setName($name): self
    {
        return $this;
    }

    public function getSku(): string
    {
        return $this->product->getSku();
    }

    public function setSku($sku): self
    {
        return $this;
    }

    public function getQuoteId(): ?int
    {
        return $this->quote->getId();
    }

    public function setQuoteId($quoteId): self
    {
        return $this;
    }

    public function getExtensionAttributes(): ?CartItemExtensionInterface
    {
        return null;
    }

    public function setExtensionAttributes(CartItemExtensionInterface $extensionAttributes): self
    {
        return $this;
    }

    public function getItemId(): ?int
    {
        return null;
    }

    public function setItemId($itemId): self
    {
        return $this;
    }

    public function getProductOption(): ?ProductOptionInterface
    {
        return null;
    }

    public function setProductOption(ProductOptionInterface $productOption): self
    {
        return $this;
    }

    public function getIsVirtual(): bool
    {
        return $this->product->isVirtual();
    }

    public function getWeight(): ?float
    {
        return null === $this->product->getWeight() ? null : (float)$this->product->getWeight();
    }

    public function getRowWeight(): ?float
    {
        return $this->getWeight();
    }

    public function getIsQtyDecimal(): bool
    {
        return false;
    }
}
