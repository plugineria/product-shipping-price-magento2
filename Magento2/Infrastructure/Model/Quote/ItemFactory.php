<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;

class ItemFactory
{
    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var AddressFactory */
    private $quoteAddressFactory;

    /** @var QuoteFactory */
    private $quoteFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        AddressFactory $quoteAddressFactory,
        QuoteFactory $quoteFactory
    ) {
        $this->productRepository = $productRepository;
        $this->quoteAddressFactory = $quoteAddressFactory;
        $this->quoteFactory = $quoteFactory;
    }

    public function create(MagentoProductId $productId, Address $address): Item
    {
        /** @var Product $product */
        $product = $this->productRepository->getById($productId->getId());

        $quote = $this->quoteFactory->create();
        $quoteAddress = $this->quoteAddressFactory->create($address, $quote);

        return new Item($product, $quoteAddress, $quote);
    }
}
