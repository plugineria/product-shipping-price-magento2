<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model;

class Volumetric
{
    /** @var float|null  */
    private $weight;

    /** @var int|null  */
    private $width;

    /** @var int|null  */
    private $height;

    /** @var int|null  */
    private $depth;

    public function __construct(
        ?float $weight = null,
        ?int $width = null,
        ?int $height = null,
        ?int $depth = null
    ) {
        $this->weight = $weight;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getDepth(): ?int
    {
        return $this->depth;
    }
}
