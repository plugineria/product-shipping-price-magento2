<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingRate;

use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Shipping\Model\CarrierFactoryInterface;
use Magento\Shipping\Model\Rate\Result;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethodId;
use Plugineria\ProductShippingPrice\Domain\Model\ShippingRate\ShippingRate;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingRate\PriceCalculator;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Factory\MagentoQuoteRateRequestFactory;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper\ShippingRateMapper;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoProductId;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\Quote\Item;

use function array_filter;
use function array_map;

class MagentoPriceCalculator implements PriceCalculator
{
    /** @var CarrierFactoryInterface */
    private $carrierFactory;

    /** @var MagentoQuoteRateRequestFactory */
    private $quoteRateRequestFactory;

    /** @var ShippingRateMapper */
    private $shippingRateMapper;

    public function __construct(
        CarrierFactoryInterface $carrierFactory,
        MagentoQuoteRateRequestFactory $quoteRateRequestFactory,
        ShippingRateMapper $shippingRateMapper
    ) {
        $this->carrierFactory = $carrierFactory;
        $this->quoteRateRequestFactory = $quoteRateRequestFactory;
        $this->shippingRateMapper = $shippingRateMapper;
    }

    public function getRatesPerShippingMethod(
        ShippingMethodId $shippingMethodId,
        ProductId $productId,
        Address $shippingAddress
    ): array {
        $productId = new MagentoProductId((int)(string)$productId);
        $shippingMethod = $this->carrierFactory->getIfActive((string)$shippingMethodId);

        if (!$shippingMethod) {
            return [];
        }

        /** @var Result|null $result */
        $rateRequest = $this->quoteRateRequestFactory->create($productId, $shippingAddress);

        /** @var Item $item */
        foreach ($rateRequest->getAllItems() as $item) {
            if ($item->getIsVirtual()) {
                return [];
            }
        }

        $result = $shippingMethod->collectRates($rateRequest);

        if (null === $result) {
            return [];
        }

        $rates = array_filter(
            $result->getAllRates(),
            static function ($rate): bool {
                return $rate instanceof Method;
            }
        );

        return array_map(
            function (Method $rate) use ($shippingMethodId): ShippingRate {
                return $this->shippingRateMapper->mageToDomain($shippingMethodId, $rate);
            },
            $rates,
        );
    }
}
