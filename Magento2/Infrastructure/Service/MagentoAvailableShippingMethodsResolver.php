<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Shipping\Model\CarrierFactoryInterface;
use Magento\Store\Model\ScopeInterface;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;
use Plugineria\ProductShippingPrice\Domain\Service\AvailableShippingMethodsResolver;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Mapper\ShippingMethodMapper;

class MagentoAvailableShippingMethodsResolver implements AvailableShippingMethodsResolver
{
    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /** @var CarrierFactoryInterface */
    private $carrierFactory;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var ShippingMethodMapper */
    private $shippingMethodMapper;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CarrierFactoryInterface $carrierFactory,
        ProductRepositoryInterface $productRepository,
        ShippingMethodMapper $shippingMethodMapper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->carrierFactory = $carrierFactory;
        $this->productRepository = $productRepository;
        $this->shippingMethodMapper = $shippingMethodMapper;
    }

    public function getAvailableShippingMethods(ProductId $productId, Address $shippingAddress): array
    {
        if (!$this->isShippableProduct($productId)) {
            return [];
        }

        $carriers = $this->scopeConfig->getValue('carriers', ScopeInterface::SCOPE_STORE);

        $shippingMethods = [];

        foreach ($carriers as $carrierCode => $carrierConfig) {
            $shippingMethod = $this->carrierFactory->getIfActive($carrierCode);

            if ($shippingMethod) {
                $shippingMethods[] = $this->shippingMethodMapper->mageToDomain($shippingMethod);
            }
        }

        return $shippingMethods;
    }

    private function isShippableProduct(ProductId $productId): bool
    {
        /** @var Product $product */
        $product = $this->productRepository->getById((int)(string)$productId);

        return $product->getTypeInstance()->hasWeight()
            && Status::STATUS_ENABLED === (int)$product->getStatus();
    }
}
