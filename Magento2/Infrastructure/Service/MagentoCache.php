<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service;

use Magento\Framework\App\CacheInterface;
use Plugineria\ProductShippingPrice\App\Service\Cache;

class MagentoCache implements Cache
{
    /** @var CacheInterface */
    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function get(string $key): ?string
    {
        $cachedValue = $this->cache->load($key);

        return $cachedValue ?: null;
    }

    public function set(string $key, string $value, ?int $ttlSec = null): void
    {
        $this->cache->save($value, $key, [], $ttlSec);
    }
}
