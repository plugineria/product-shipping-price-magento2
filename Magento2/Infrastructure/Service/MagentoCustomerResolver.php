<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Session;

class MagentoCustomerResolver
{
    /** @var Session */
    private $customerSession;

    /** @var CustomerRepositoryInterface */
    private $customerRepository;

    public function __construct(Session $customerSession, CustomerRepositoryInterface $customerRepository)
    {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }

    public function getCurrentCustomer(): ?CustomerInterface
    {
        $customerId = $this->customerSession->getCustomerId();

        if ($customerId) {
            return $this->customerRepository->getById($customerId);
        }

        return null;
    }
}
