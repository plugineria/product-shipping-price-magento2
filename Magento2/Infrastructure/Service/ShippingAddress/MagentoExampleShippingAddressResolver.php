<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingAddress;

use Magento\Directory\Model\RegionFactory;
use Plugineria\ProductShippingPrice\Domain\Model\Address\Address;
use Plugineria\ProductShippingPrice\Domain\Model\Address\DefaultAddress;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\ExampleShippingAddressResolver;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Config\ExampleShippingAddressConfig;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Config\OriginAddressConfig;

class MagentoExampleShippingAddressResolver implements ExampleShippingAddressResolver
{
    /** @var ExampleShippingAddressConfig */
    private $exampleShippingAddressConfig;

    /** @var OriginAddressConfig */
    private $originAddressConfig;

    /** @var RegionFactory */
    private $regionFactory;

    public function __construct(
        ExampleShippingAddressConfig $exampleShippingAddressConfig,
        OriginAddressConfig $originAddressConfig,
        RegionFactory $regionFactory
    ) {
        $this->exampleShippingAddressConfig = $exampleShippingAddressConfig;
        $this->originAddressConfig = $originAddressConfig;
        $this->regionFactory = $regionFactory;
    }

    public function getExampleShippingAddress(): Address
    {
        $country = $this->exampleShippingAddressConfig->getCountry() ?: $this->originAddressConfig->getCountry();
        $regionId = $this->exampleShippingAddressConfig->getRegion() ?: $this->originAddressConfig->getRegionId();
        $region = $this->regionFactory->create()->load($regionId);

        return new DefaultAddress(
            $country,
            $this->exampleShippingAddressConfig->getCity() ?: $this->originAddressConfig->getCity(),
            $this->exampleShippingAddressConfig->getPostalCode()
                ?: $this->originAddressConfig->getPostalCode(),
            $this->exampleShippingAddressConfig->getStreet() ?: null,
            $regionId ? $region->getName() : null
        );
    }
}
