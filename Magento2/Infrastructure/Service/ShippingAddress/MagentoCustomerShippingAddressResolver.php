<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service\ShippingAddress;

use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\ShippingAddress\CustomerShippingAddress;
use Plugineria\ProductShippingPrice\Domain\Repository\CustomerShippingAddressRepository;
use Plugineria\ProductShippingPrice\Domain\Service\ShippingAddress\CustomerShippingAddressResolver;

class MagentoCustomerShippingAddressResolver implements CustomerShippingAddressResolver
{
    /** @var CustomerShippingAddressRepository */
    private $customerShippingAddressRepository;

    public function __construct(CustomerShippingAddressRepository $customerShippingAddressRepository)
    {
        $this->customerShippingAddressRepository = $customerShippingAddressRepository;
    }

    public function getPrimaryCustomerShippingAddress(CustomerId $customerId): ?CustomerShippingAddress
    {
        $customerAddresses = $this->customerShippingAddressRepository->findAllByCustomerId($customerId);

        return [] === $customerAddresses ? null : reset($customerAddresses);
    }
}
