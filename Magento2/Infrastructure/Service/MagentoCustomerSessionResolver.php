<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Service;

use Magento\Customer\Model\Session;
use Plugineria\ProductShippingPrice\Domain\Model\Customer\CustomerId;
use Plugineria\ProductShippingPrice\Domain\Service\CustomerSessionResolver;
use Plugineria\ProductShippingPrice\Magento2\Infrastructure\Model\MagentoCustomerId;

class MagentoCustomerSessionResolver implements CustomerSessionResolver
{
    /** @var Session */
    private $customerSession;

    public function __construct(Session $customerSession)
    {
        $this->customerSession = $customerSession;
    }

    public function getCustomerId(): ?CustomerId
    {
        $customerId = $this->customerSession->getCustomerId();

        return $customerId ? new MagentoCustomerId($customerId) : null;
    }
}
