<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class ExampleShippingAddressConfig
{
    private const XML_PATH_COUNTRY = 'plugineria_product_shipping_price/example_shipping_address/country';
    private const XML_PATH_REGION = 'plugineria_product_shipping_price/example_shipping_address/region_id';
    private const XML_PATH_POSTCODE = 'plugineria_product_shipping_price/example_shipping_address/postcode';
    private const XML_PATH_CITY = 'plugineria_product_shipping_price/example_shipping_address/city';
    private const XML_PATH_STREET = 'plugineria_product_shipping_price/example_shipping_address/street';

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getCountry(): ?string
    {
        return $this->scopeConfig->getValue(static::XML_PATH_COUNTRY, ScopeInterface::SCOPE_STORE);
    }

    public function getRegion(): ?int
    {
        $regionId = $this->scopeConfig->getValue(static::XML_PATH_REGION, ScopeInterface::SCOPE_STORE);

        return null === $regionId ? null : (int)$regionId;
    }

    public function getPostalCode(): ?string
    {
        return $this->scopeConfig->getValue(static::XML_PATH_POSTCODE, ScopeInterface::SCOPE_STORE);
    }

    public function getCity(): ?string
    {
        return $this->scopeConfig->getValue(static::XML_PATH_CITY, ScopeInterface::SCOPE_STORE);
    }

    public function getStreet(): ?string
    {
        return $this->scopeConfig->getValue(static::XML_PATH_STREET, ScopeInterface::SCOPE_STORE);
    }
}
