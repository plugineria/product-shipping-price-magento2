<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Infrastructure\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Shipping\Model\Config;
use Magento\Store\Model\ScopeInterface;

class OriginAddressConfig
{
    /** @var ScopeConfigInterface */
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getCountry(): string
    {
        return (string)$this->scopeConfig->getValue(
            Config::XML_PATH_ORIGIN_COUNTRY_ID,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getRegionId(): ?int
    {
        $regionId = $this->scopeConfig->getValue(
            Config::XML_PATH_ORIGIN_REGION_ID,
            ScopeInterface::SCOPE_STORE
        );

        return null === $regionId ? null : (int)$regionId;
    }

    public function getPostalCode(): string
    {
        return (string)$this->scopeConfig->getValue(
            Config::XML_PATH_ORIGIN_POSTCODE,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getCity(): string
    {
        return (string)$this->scopeConfig->getValue(
            Config::XML_PATH_ORIGIN_CITY,
            ScopeInterface::SCOPE_STORE
        );
    }
}
