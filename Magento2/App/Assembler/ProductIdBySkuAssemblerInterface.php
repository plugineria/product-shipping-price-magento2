<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\App\Assembler;

use Magento\Framework\Exception\NoSuchEntityException;
use Plugineria\ProductShippingPrice\Domain\Model\ProductId;

interface ProductIdBySkuAssemblerInterface
{
    /**
     * @param string $sku
     *
     * @return ProductId
     *
     * @throws NoSuchEntityException
     */
    public function getProductIdBySku(string $sku): ProductId;
}
