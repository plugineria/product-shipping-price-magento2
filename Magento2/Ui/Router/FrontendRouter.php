<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Router;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

use function array_map;
use function array_merge;
use function array_splice;
use function class_exists;
use function count;
use function explode;
use function implode;
use function urldecode;

class FrontendRouter implements RouterInterface
{
    private const NAMESPACE_DELIMITER = '\\';

    /** @var ActionFactory */
    private $actionFactory;

    public function __construct(ActionFactory $actionFactory)
    {
        $this->actionFactory = $actionFactory;
    }

    /**
     * @param Http $request
     *
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        [[$module, $actionPath, $actionName], $variables] = $this->parseRequest($request->getPathInfo());
        $actionClass = $this->getActionClass($module, $actionPath, $actionName);

        if (class_exists($actionClass)) {
            $action = $this->actionFactory->create($actionClass);

            $request->setModuleName($module);
            $request->setControllerName($actionPath);
            $request->setActionName($actionName);
            $request->setControllerModule($module);
            $request->setRouteName($module);
            if ([] !== $variables) {
                $request->setParams($variables);
            }

            return $action;
        }

        return null;
    }

    private function getActionClass(string ...$controllerArgs): string
    {
        return implode(
            self::NAMESPACE_DELIMITER,
            array_merge(
                $this->getControllerNamespace(),
                array_map('ucfirst', $controllerArgs),
            )
        );
    }

    private function getControllerNamespace(): array
    {
        $namespace = explode(self::NAMESPACE_DELIMITER, __NAMESPACE__);
        array_pop($namespace);
        $namespace[] = 'Controller';

        return $namespace;
    }

    private function parseRequest(string $requestPath): array
    {
        $path = trim($requestPath, '/');
        $params = explode('/', $path);
        $controllerArgs = array_splice($params, 0, 3);
        $variables = [];

        for ($i = 0, $l = count($params); $i < $l; $i += 2) {
            $variables[$params[$i]] = isset($params[$i + 1]) ? urldecode($params[$i + 1]) : '';
        }

        return [$controllerArgs, $variables];
    }
}
