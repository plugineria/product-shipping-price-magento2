<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data;

use Plugineria\ProductShippingPrice\Domain\Model\ShippingMethod\ShippingMethod;

class ShippingMethodDecorator implements ShippingMethodInterface
{
    /** @var ShippingMethod */
    private $shippingMethod;

    public function __construct(ShippingMethod $shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    public function getId(): string
    {
        return (string)$this->shippingMethod->getId();
    }

    public function getTitle(): string
    {
        return $this->shippingMethod->getTitle();
    }
}
