<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data;

use Plugineria\ProductShippingPrice\App\View\ShippingRateView;

class ShippingRateDecorator implements ShippingRateInterface
{
    /** @var ShippingRateView */
    private $shippingRate;

    public function __construct(ShippingRateView $shippingRate)
    {
        $this->shippingRate = $shippingRate;
    }

    public function getPrice(): float
    {
        return $this->shippingRate->getPrice();
    }

    public function getCode(): string
    {
        return $this->shippingRate->getCode();
    }

    public function getTitle(): string
    {
        return (string)$this->shippingRate->getTitle();
    }

    public function getDescription(): ?string
    {
        return $this->shippingRate->getDescription();
    }

    public function getShippingMethod(): ShippingMethodInterface
    {
        return new ShippingMethodDecorator($this->shippingRate->getShippingMethod());
    }
}
