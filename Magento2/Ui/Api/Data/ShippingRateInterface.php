<?php

declare(strict_types=1);

// phpcs:disable SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data;

/**
 * @api
 *
 * @since 0.1.0
 */
interface ShippingRateInterface
{
    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @return \Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingMethodInterface
     */
    public function getShippingMethod(): ShippingMethodInterface;
}
