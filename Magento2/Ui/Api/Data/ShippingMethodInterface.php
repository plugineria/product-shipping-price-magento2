<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data;

/**
 * @api
 *
 * @since 0.1.0
 */
interface ShippingMethodInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getTitle(): string;
}
