<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Plugineria\ProductShippingPrice\App\Query\ProductShippingRatesQueryPort;
use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Magento2\App\Assembler\ProductIdBySkuAssemblerInterface;
use Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateDecorator;
use Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateInterface;

class ProductShippingRates implements ProductShippingRatesInterface
{
    /** @var ProductIdBySkuAssemblerInterface */
    private $productIdBySkuAssembler;

    /** @var ProductShippingRatesQueryPort */
    private $productShippingRatesQuery;

    public function __construct(
        ProductIdBySkuAssemblerInterface $productIdBySkuAssembler,
        ProductShippingRatesQueryPort $productShippingRatesQuery
    ) {
        $this->productIdBySkuAssembler = $productIdBySkuAssembler;
        $this->productShippingRatesQuery = $productShippingRatesQuery;
    }

    /**
     * @param string $sku
     *
     * @return ShippingRateInterface[]
     *
     * @throws NoSuchEntityException
     */
    public function getShippingRates(string $sku): array
    {
        $productId = $this->productIdBySkuAssembler->getProductIdBySku($sku);
        $shippingRates = $this->productShippingRatesQuery->execute($productId);

        return array_map(
            static function (ShippingRateView $shippingRate): ShippingRateDecorator {
                return new ShippingRateDecorator($shippingRate);
            },
            $shippingRates
        );
    }
}
