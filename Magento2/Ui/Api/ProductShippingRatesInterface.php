<?php

declare(strict_types=1);

// phpcs:disable SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @api
 *
 * @since 0.1.0
 */
interface ProductShippingRatesInterface
{
    /**
     * @param string $sku
     *
     * @return \Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateInterface[]
     *
     * @throws NoSuchEntityException
     */
    public function getShippingRates(string $sku): array;
}
