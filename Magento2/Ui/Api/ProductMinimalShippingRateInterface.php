<?php

declare(strict_types=1);

// phpcs:disable SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateInterface;

/**
 * @api
 *
 * @since 0.1.0
 */
interface ProductMinimalShippingRateInterface
{
    /**
     * @param string $sku
     *
     * @return \Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateInterface|null
     *
     * @throws NoSuchEntityException
     */
    public function getMinimalShippingRate(string $sku): ?ShippingRateInterface;
}
