<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Plugineria\ProductShippingPrice\App\Query\ProductMinimalShippingRateQueryPort;
use Plugineria\ProductShippingPrice\Magento2\App\Assembler\ProductIdBySkuAssemblerInterface;
use Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateDecorator;
use Plugineria\ProductShippingPrice\Magento2\Ui\Api\Data\ShippingRateInterface;

class ProductMinimalShippingRate implements ProductMinimalShippingRateInterface
{
    /** @var ProductIdBySkuAssemblerInterface */
    private $productIdBySkuAssembler;

    /** @var ProductMinimalShippingRateQueryPort */
    private $productMinimalShippingRateQuery;

    public function __construct(
        ProductIdBySkuAssemblerInterface $productIdBySkuAssembler,
        ProductMinimalShippingRateQueryPort $productMinimalShippingRateQuery
    ) {
        $this->productIdBySkuAssembler = $productIdBySkuAssembler;
        $this->productMinimalShippingRateQuery = $productMinimalShippingRateQuery;
    }

    /**
     * @param string $sku
     *
     * @return ShippingRateInterface|null
     *
     * @throws NoSuchEntityException
     */
    public function getMinimalShippingRate(string $sku): ?ShippingRateInterface
    {
        $productId = $this->productIdBySkuAssembler->getProductIdBySku($sku);
        $shippingRate = $this->productMinimalShippingRateQuery->execute($productId);

        return $shippingRate ? new ShippingRateDecorator($shippingRate) : null;
    }
}
