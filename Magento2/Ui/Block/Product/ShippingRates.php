<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Block\Product;

use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\View\Element\Template;
use Plugineria\ProductShippingPrice\App\Query\ProductShippingRatesQueryPort;
use Plugineria\ProductShippingPrice\App\Query\SessionShippingAddressQueryPort;
use Plugineria\ProductShippingPrice\App\View\SessionShippingAddressView;
use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\DefaultProductId;

class ShippingRates extends Template
{
    /** @var ProductShippingRatesQueryPort */
    private $shippingRatesQuery;

    /** @var SessionShippingAddressQueryPort */
    private $sessionShippingAddressQuery;

    /** @var Data */
    private $priceHelper;

    public function __construct(
        ProductShippingRatesQueryPort $shippingRatesQuery,
        SessionShippingAddressQueryPort $sessionShippingAddressQuery,
        Data $priceHelper,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->shippingRatesQuery = $shippingRatesQuery;
        $this->sessionShippingAddressQuery = $sessionShippingAddressQuery;
        $this->priceHelper = $priceHelper;
    }

    public function getPriceHelper(): Data
    {
        return $this->priceHelper;
    }

    public function getSessionShippingAddress(): SessionShippingAddressView
    {
        return $this->sessionShippingAddressQuery->execute();
    }

    /**
     * @return ShippingRateView[]
     */
    public function getRates(): array
    {
        return $this->shippingRatesQuery->execute($this->getProductId());
    }

    private function getProductId(): DefaultProductId
    {
        return new DefaultProductId($this->getRequest()->getParam('id'));
    }
}
