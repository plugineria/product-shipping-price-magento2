<?php

declare(strict_types=1);

namespace Plugineria\ProductShippingPrice\Magento2\Ui\Block\Product;

use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\View\Element\Template;
use Plugineria\ProductShippingPrice\App\Query\ProductMinimalShippingRateQueryPort;
use Plugineria\ProductShippingPrice\App\Query\SessionShippingAddressQueryPort;
use Plugineria\ProductShippingPrice\App\View\SessionShippingAddressView;
use Plugineria\ProductShippingPrice\App\View\ShippingRateView;
use Plugineria\ProductShippingPrice\Domain\Model\DefaultProductId;

class MinimalShippingRate extends Template
{
    /** @var ProductMinimalShippingRateQueryPort */
    private $productMinimalShippingRateQuery;

    /** @var SessionShippingAddressQueryPort */
    private $sessionShippingAddressQuery;

    /** @var Data */
    private $priceHelper;

    public function __construct(
        Template\Context $context,
        ProductMinimalShippingRateQueryPort $productMinimalShippingRateQuery,
        SessionShippingAddressQueryPort $sessionShippingAddressQuery,
        Data $priceHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->productMinimalShippingRateQuery = $productMinimalShippingRateQuery;
        $this->sessionShippingAddressQuery = $sessionShippingAddressQuery;
        $this->priceHelper = $priceHelper;
    }

    public function getMinimalShippingRate(): ?ShippingRateView
    {
        return $this->productMinimalShippingRateQuery->execute($this->getProductId());
    }

    public function getSessionShippingAddress(): SessionShippingAddressView
    {
        return $this->sessionShippingAddressQuery->execute();
    }

    public function getPriceHelper(): Data
    {
        return $this->priceHelper;
    }

    public function getAllProductRatesUrl(): string
    {
        return $this->getUrl('catalog/product/shippingRates', ['id' => (string)$this->getProductId()]);
    }

    private function getProductId(): DefaultProductId
    {
        return new DefaultProductId($this->getRequest()->getParam('id'));
    }
}
