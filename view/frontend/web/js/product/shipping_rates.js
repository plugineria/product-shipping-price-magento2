"use strict";

define(
    ["jquery", "mage/translate", "Magento_Ui/js/modal/modal"],
    function ($, $t) {
        return function(config, element) {
            let shippingRatesModals = {};

            $(element).click(function(event) {
                event.preventDefault();

                if (this.href in shippingRatesModals) {
                    shippingRatesModals[this.href].modal("openModal");
                } else {
                    $(element).trigger("processStart");

                    $.get(this.href).done((html) => {
                        shippingRatesModals[this.href] = $("<div/>").append($.parseHTML(html)).find("#maincontent");

                        $(element).trigger("processStop");
                        shippingRatesModals[this.href].modal({
                            "title": $t("Shipping"),
                            "autoOpen": true
                        });
                    });
                }
            });
        }
    }
)