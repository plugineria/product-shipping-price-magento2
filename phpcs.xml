<?xml version="1.0"?>
<ruleset name="plugineria_magento2">
    <config name="installed_paths" value="../../slevomat/coding-standard"/>
    <arg name="basepath" value="."/>
    <arg name="extensions" value="php"/>
    <arg name="parallel" value="45"/>
    <arg name="cache" value=".phpcs-cache"/>
    <arg name="colors"/>

    <file>Magento2</file>
    <file>Test</file>

    <rule ref="PSR12">
        <exclude name="PSR2.Methods.MethodDeclaration.Underscore"/>
    </rule>

    <!-- Line length -->
    <rule ref="Generic.Files.LineLength">
        <properties>
            <property name="absoluteLineLimit" value="120" />
        </properties>
    </rule>

    <!-- Arrays -->
    <rule ref="Generic.Arrays.DisallowLongArraySyntax"/>
    <rule ref="SlevomatCodingStandard.Arrays.DisallowImplicitArrayCreation"/>
    <!-- <rule ref="SlevomatCodingStandard.Arrays.TrailingArrayComma"/> --> <!-- :> -->

    <!-- Classes -->
    <rule ref="SlevomatCodingStandard.Classes.ClassConstantVisibility"/>
    <rule ref="SlevomatCodingStandard.Classes.EmptyLinesAroundClassBraces">
        <properties>
            <property name="linesCountAfterOpeningBrace" value="0"/>
            <property name="linesCountBeforeClosingBrace" value="0"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Classes.ModernClassNameReference"/>
    <rule ref="SlevomatCodingStandard.Classes.TraitUseDeclaration"/>
    <rule ref="SlevomatCodingStandard.Classes.UnusedPrivateElements"/>

    <!-- Commenting -->
    <rule ref="SlevomatCodingStandard.Commenting.ForbiddenAnnotations">
        <properties>
            <property name="forbiddenAnnotations" type="array">
                <element value="@author"/>
                <element value="@created"/>
                <element value="@version"/>
                <element value="@package"/>
                <element value="@copyright"/>
                <element value="@license"/>
            </property>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Commenting.ForbiddenComments">
        <properties>
            <property name="forbiddenCommentPatterns" type="array">
                <element value="/Constructor\./i"/>
                <element value="/Created by PhpStorm\./i"/>
            </property>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Commenting.DocCommentSpacing">
        <properties>
            <property name="linesCountBetweenDifferentAnnotationsTypes" value="1"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Commenting.EmptyComment"/>
    <!-- seems to be broken in 6.1: <rule ref="SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration"/> -->
    <rule ref="SlevomatCodingStandard.Commenting.UselessInheritDocComment"/>
    <rule ref="SlevomatCodingStandard.Commenting.RequireOneLinePropertyDocComment"/>

    <!-- ControlStructures -->
    <rule ref="SlevomatCodingStandard.ControlStructures.AssignmentInCondition"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.DisallowContinueWithoutIntegerOperandInSwitch"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.DisallowEmpty"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.LanguageConstructWithParentheses"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.NewWithParentheses"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireShortTernaryOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.UselessTernaryOperator"/>

    <!-- Exceptions -->
    <rule ref="SlevomatCodingStandard.Exceptions.DeadCatch"/>
    <!-- <rule ref="SlevomatCodingStandard.Exceptions.ReferenceThrowableOnly"/> -->

    <!-- Functions -->
    <rule ref="SlevomatCodingStandard.Functions.StaticClosure"/>
    <rule ref="SlevomatCodingStandard.Functions.UnusedInheritedVariablePassedToClosure"/>

    <!-- Namespaces -->
    <rule ref="SlevomatCodingStandard.Namespaces.AlphabeticallySortedUses"/>
    <rule ref="SlevomatCodingStandard.Namespaces.MultipleUsesPerLine"/>
    <rule ref="SlevomatCodingStandard.Namespaces.NamespaceSpacing"/>
    <rule ref="SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly">
        <properties>
            <property name="searchAnnotations" value="true"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Namespaces.RequireOneNamespaceInFile"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UnusedUses">
        <properties>
            <property name="searchAnnotations" value="true"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Namespaces.UseFromSameNamespace"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UselessAlias"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UseSpacing">
        <properties>
            <property name="linesCountBetweenUseTypes" value="1"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Namespaces.UseDoesNotStartWithBackslash"/>

    <!-- Metrics -->
    <rule ref="Generic.Metrics.CyclomaticComplexity">
        <properties>
            <property name="complexity" value="5"/>
            <property name="absoluteComplexity" value="5"/>
        </properties>
    </rule>
    <rule ref="Generic.Metrics.NestingLevel">
        <properties>
            <property name="nestingLevel" value="3"/>
            <property name="absoluteNestingLevel" value="3"/>
        </properties>
    </rule>

    <!-- PHP -->
    <rule ref="Generic.PHP.ForbiddenFunctions">
        <properties>
            <property name="forbiddenFunctions" type="array">
                <element key="is_null" value="null"/>
                <element key="dump" value="null"/>
            </property>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.PHP.UselessSemicolon"/>
    <rule ref="Squiz.PHP.DiscouragedFunctions">
        <properties>
            <property name="error" value="true"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.PHP.ShortList"/>
    <!-- shorthand names in type casts -->
    <rule ref="SlevomatCodingStandard.PHP.TypeCast"/>
    <rule ref="SlevomatCodingStandard.PHP.UselessParentheses"/>

    <!-- Operators -->
    <rule ref="SlevomatCodingStandard.Operators.DisallowEqualOperators"/>
    <rule ref="SlevomatCodingStandard.Operators.SpreadOperatorSpacing"/>

    <!-- Type hints -->
    <rule ref="SlevomatCodingStandard.TypeHints.DeclareStrictTypes">
        <properties>
            <property name="newlinesCountBetweenOpenTagAndDeclare" value="2"/>
            <property name="newlinesCountAfterDeclare" value="2"/>
            <property name="spacesCountAroundEqualsSign" value="0"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.TypeHints.LongTypeHints"/>
    <rule ref="SlevomatCodingStandard.TypeHints.NullTypeHintOnLastPosition"/>
    <rule ref="SlevomatCodingStandard.TypeHints.ReturnTypeHintSpacing"/>
    <rule ref="SlevomatCodingStandard.TypeHints.ParameterTypeHintSpacing"/>

    <!-- Variables -->
    <rule ref="SlevomatCodingStandard.Variables.UnusedVariable">
        <properties>
            <property name="ignoreUnusedValuesWhenOnlyKeysAreUsedInForeach" value="true"/>
        </properties>
    </rule>

    <!-- White space -->
    <!-- There must be a single space after following language constructs: -->
    <!-- echo, print, return, include*, require*, new -->
    <rule ref="Squiz.WhiteSpace.LanguageConstructSpacing"/>
    <rule ref="Squiz.WhiteSpace.SuperfluousWhitespace"/>

    <rule ref="vendor/magento/magento-coding-standard/Magento2/ruleset.xml"/>
</ruleset>
